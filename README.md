This is a project template for C#.NET projects based on standard folder structure with prepared CI pipeline. It has built-in capabilities for build, test, coverage and metrics calculation, and code inspection monitoring. All outcomes of pipeline are handled by GitLab and are shown in MR panel. This sped up review and allow to track code quality continiously. The template supports both code bases: legacy on .NET Framework 4.5 or less and cross-platform on .NET Core (.NET Standard). Of course you can use one these frameworks as the target. 

Before you start using template prepare build machine by installing the following toolset. It is stored here: https://gitlab.com/almazsr/CITools. It includes:
* NuGet (to restore .NET Framework packages)
* OpenCover (.NET Framework coverage tool)
* ReportGenerator (tool that can merge multiple coverage reports into different output formats)
* Metrics (tool by Microsoft to calculate the metrics)

This toolset also has 2 self-written tools that converts JetBrains InspectCode and Microsoft Metrics formats to CodeClimate and OpenMetrics respectively.
* ConvertInspectCodeToCodeClimate
* ConvertMicrosoftMetricsToOpenMetrics

To get everything ready, start build/build.ps1 script from the repository. After successful build grab everything from bin folder and put it on your build machine.

Entry point to whole CI pipeline is PowerShell script build/build.ps1. The script has a readable structure that is broken down into functions. You can get rid of unnecessary things there and add your own calls. Pipeline of the script is following:
* build
* inspect code (running InspectCode and converting the results to CodeClimate format)
* calculate code metrics (running Microsoft Metrics tool and converting the results to OpenMetrics format) 
* test (running the tests)
* calculate coverage (running the tests with OpenCover and Coverlet)

To start using the template do the following steps:
1. Install required toolset to your build machine from https://gitlab.com/almazsr/CITools.
2. Enable coverage results showing in MR panel in GitLab by setting "Test coverage parsing" (Settings -> CI / CD Settings -> General pipelines) to `"linecoverage": ([0-9.]+).`
3. Change the build/build.ps1 PowerShell script according to your solution paths. 
4. Push the changes.
